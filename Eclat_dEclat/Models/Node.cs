﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eclat_dEclat.Models
{
    public class Node
    {
        public List<int> ItemSetIDs { get; }
        public List<int> TransactionIDs { get; }
        public int NodeSupport { get; }

        public Node(List<int> itemSetIDs, List<int> transactionIDs, int nodeSupport)
        {
            ItemSetIDs = itemSetIDs;
            TransactionIDs = transactionIDs;
            NodeSupport = nodeSupport;
        }
    }
}
