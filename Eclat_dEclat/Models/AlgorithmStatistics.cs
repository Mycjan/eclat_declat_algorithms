﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eclat_dEclat.Models
{
    public class AlgorithmStatistics
    {
        public string DatasetName { get; set; }
        public int DistinctItemsCount { get; set; }
        public int MinimalSupport { get; set; }
        public List<int> SelectedItems { get; set; }
        public Dictionary<int, int> CreatedCandidatesCount { get; set; }
        public Dictionary<int, int> DiscoveredFrequentItemsetsCount { get; set; }   
        public TimeSpan InputReadingTime { get; set; }
        public TimeSpan DatasetReadingTime { get; set; }
        public Dictionary<int, TimeSpan> TreeLevelGenerationTime { get; set; }
        public TimeSpan OutSavingTime { get; set; }

        public AlgorithmStatistics()
        {
            SelectedItems = new();
            CreatedCandidatesCount = new();
            DiscoveredFrequentItemsetsCount = new();
            TreeLevelGenerationTime = new();
        }

        public void SaveStatistics(string path, Stopwatch globalTimer)
        {
            Stopwatch statisticsSavingTimer = new();
            statisticsSavingTimer.Start();

            FileStream statsFile = File.Open(path, FileMode.OpenOrCreate);
            StreamWriter statsFileWriter = new(statsFile);

            statsFileWriter.WriteLine($"Dataset name: {DatasetName}");
            statsFileWriter.WriteLine($"Distince items count: {DistinctItemsCount}");
            statsFileWriter.WriteLine($"Minimal support: {MinimalSupport}");
            if (SelectedItems == null)
                statsFileWriter.WriteLine($"Selected items: all");
            else
                statsFileWriter.WriteLine($"Selected items: {string.Join(", ", SelectedItems)}");

            SaveCreatedCandidatesAndItemsets(statsFileWriter);
            SaveRuntimes(statsFileWriter, statisticsSavingTimer, globalTimer);

            statsFileWriter.Close();
        }

        private void SaveRuntimes(StreamWriter statsFileWriter, Stopwatch statisticsSavingTimer, Stopwatch globalTimer)
        {
            statsFileWriter.WriteLine($"Input reading time: {InputReadingTime}");
            statsFileWriter.WriteLine($"Dataset reading time: {DatasetReadingTime}");
            TimeSpan algorithmRuntime = new (0);
            foreach (int key in TreeLevelGenerationTime.Keys)
            {
                statsFileWriter.WriteLine($"Discovering itemsets of length {key} time: {TreeLevelGenerationTime[key]}");
                algorithmRuntime += TreeLevelGenerationTime[key];
            }

            statsFileWriter.WriteLine($"Algorithm total runtime: {algorithmRuntime}");
            statsFileWriter.WriteLine($"Saving out file time: {OutSavingTime}");

            statisticsSavingTimer.Stop();
            statsFileWriter.WriteLine($"Saving stats file time: {statisticsSavingTimer.Elapsed}");

            globalTimer.Stop();
            statsFileWriter.WriteLine($"Total runtime: {globalTimer.Elapsed}");
        }

        private void SaveCreatedCandidatesAndItemsets(StreamWriter statsFileWriter)
        {
            int discoveredFrequentSum = DiscoveredFrequentItemsetsCount.Values.Sum();
            int createdCandidatesSum = CreatedCandidatesCount.Values.Sum();

            double frequentLengthsSum = 0;
            foreach (int key in DiscoveredFrequentItemsetsCount.Keys)
                frequentLengthsSum += key * DiscoveredFrequentItemsetsCount[key];
            double avarageFrequentLength = frequentLengthsSum / discoveredFrequentSum;

            double candidatesLengthsSum = 0;
            foreach (int key in CreatedCandidatesCount.Keys)
                candidatesLengthsSum += key * CreatedCandidatesCount[key];
            double avarageCandidateLength = candidatesLengthsSum / createdCandidatesSum;

            double frequentStandardDeviation = 0;
            foreach (int key in DiscoveredFrequentItemsetsCount.Keys)
                frequentStandardDeviation += DiscoveredFrequentItemsetsCount[key] * ((key - avarageFrequentLength) * (key - avarageFrequentLength));
            frequentStandardDeviation = Math.Sqrt(frequentStandardDeviation / (discoveredFrequentSum - 1));

            double candidatesStandardDeviation = 0;
            foreach (int key in CreatedCandidatesCount.Keys)
                candidatesStandardDeviation += CreatedCandidatesCount[key] * ((key - avarageCandidateLength) * (key - avarageCandidateLength));
            candidatesStandardDeviation = Math.Sqrt(candidatesStandardDeviation / (createdCandidatesSum - 1));

            statsFileWriter.WriteLine($"Total frequent itemsets count: {discoveredFrequentSum}, total created candidates count: {createdCandidatesSum}");
            statsFileWriter.WriteLine($"Number of items in longest itemset: {DiscoveredFrequentItemsetsCount.Keys.Max()}");
            statsFileWriter.WriteLine($"Avarage frequent itemsets length: {avarageFrequentLength}, avarage candidate itemsets length: {avarageCandidateLength}");
            statsFileWriter.WriteLine($"Standatd deviation of frequent itemsets length: {frequentStandardDeviation}, standard deviation of candidate itemsets length: {candidatesStandardDeviation}");

            foreach (int key in CreatedCandidatesCount.Keys)
                statsFileWriter.WriteLine($"Created candidates of length {key} count: {CreatedCandidatesCount[key]}");

            foreach (int key in DiscoveredFrequentItemsetsCount.Keys)
                statsFileWriter.WriteLine($"Discovered frequent itemsets of length {key} count: {DiscoveredFrequentItemsetsCount[key]}");
        }
    }
}
