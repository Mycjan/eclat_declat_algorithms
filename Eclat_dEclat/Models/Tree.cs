﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eclat_dEclat.Models
{
    public class Tree
    {
        public List<List<Node>> TreeNodes { get; } //each list contains nodes from one level

        public Tree()
        {
            TreeNodes = new List<List<Node>>();
        }

        public int AddLevel()
        {
            TreeNodes.Add(new List<Node>());
            return TreeNodes.Count - 1;
        }

        public void RemoveLastLevel()
        {
            TreeNodes.RemoveAt(TreeNodes.Count - 1);
        }

        public void AddNode(Node node)
        {
            TreeNodes[^1].Add(node);
        }

        public void SaveOutput(string path, AlgorithmStatistics statistics = null)
        {
            Stopwatch outSavingTime = new();
            if (statistics != null)
                outSavingTime.Start();

            FileStream outFile = File.Open(path, FileMode.OpenOrCreate);
            StreamWriter fileWriter = new(outFile);
            fileWriter.WriteLine("Length; NodeSupport; DiscoveredFrequentItemset");

            for (int i = 0; i < TreeNodes.Count; i++)
                for (int j = 0; j < TreeNodes[i].Count; j++)
                    fileWriter.WriteLine($"{TreeNodes[i][j].ItemSetIDs.Count}; {TreeNodes[i][j].NodeSupport}; {string.Join(", ", TreeNodes[i][j].ItemSetIDs.ToArray())}");

            fileWriter.Close();

            if (statistics != null)
            {
                outSavingTime.Stop();
                statistics.OutSavingTime = outSavingTime.Elapsed;
            }
        }
    }
}
