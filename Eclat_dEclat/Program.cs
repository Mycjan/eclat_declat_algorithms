﻿using Eclat_dEclat.Algorithms;
using Eclat_dEclat.Enums;
using Eclat_dEclat.Models;
using Eclat_dEclat.Parser;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;

namespace Eclat_dEclat
{
    class Program
    {
        static void Main(string[] args)
        {
            Stopwatch globalTimer = new();
            globalTimer.Start();

            AlgorithmStatistics algorithmStatistics = new();
            (string datasetName, int minimalSupport, List<int> itemsOfInteres, AlgorithmType algorithmType) = GetInputData(algorithmStatistics);
            algorithmStatistics.DatasetName = datasetName;
            algorithmStatistics.MinimalSupport = minimalSupport;
            algorithmStatistics.DistinctItemsCount = DatasetParser.datasetsDictionary[datasetName].distinctElementsCount;

            Console.WriteLine("Parsing dataset...");
            Dictionary<int, List<int>> datasetTransactions = DatasetParser.ParseDataset(datasetName, algorithmStatistics);

            Console.WriteLine("Calculating...");
            FIsAlgorithm algorithm = FIsAlgorithm.CreateAlgorithmInstance(algorithmType, datasetTransactions, itemsOfInteres, algorithmStatistics);
            Tree frequentItemsets = algorithm.GenerateFIs(minimalSupport);
            string fileDescription = $"{(algorithmType == AlgorithmType.dEclat ? "dEclat" : "Eclat")}_" +
                $"{datasetName}_" +
                $"t{algorithm.TransactionsCount}_" +
                $"m{minimalSupport}_" +
                $"{(itemsOfInteres == null ? "all" : itemsOfInteres.Count)}Items";
            frequentItemsets.SaveOutput($"out_" + fileDescription + $".csv", algorithmStatistics);
            algorithmStatistics.SaveStatistics($"stats_" + fileDescription + ".txt", globalTimer);
        }

        private static (string datasetName, int minimalSupport, List<int> itemsOfInteres, AlgorithmType algorithmType) GetInputData(AlgorithmStatistics statistics = null)
        {
            Stopwatch inputReadingTime = new();
            if (statistics != null)
                inputReadingTime.Start();

            AlgorithmType algorithmType = GetAlgorithmType();
            string datasetName = GetDatasetName();
            int minimalSupport = GetMinimalSupport();
            List<int> itemsOfInterest = GetItemsOfInterest();

            if (statistics != null)
            {
                inputReadingTime.Stop();
                statistics.InputReadingTime = inputReadingTime.Elapsed;
            }

            return (datasetName, minimalSupport, itemsOfInterest, algorithmType);
        }

        private static List<int> GetItemsOfInterest()
        {
            Console.WriteLine("Items of interest:\n1. Use all items\n2. Specify items ids\n3. Specify path to file with ids delimited by ','");
            int type = 0;
            while (type < 1 || type > 3)
            {
                Console.WriteLine("Insert number 1 to 3:");
                string line = Console.ReadLine();
                bool parsed = int.TryParse(line, out type);

                if (!parsed)
                    continue;
            }

            switch (type)
            {
                case 1:
                    return null;
                case 2:
                    Console.WriteLine("Specify items ids delimited by ',' in line:");
                    break;
                case 3:
                    Console.WriteLine("Specify path to file:");
                    break;
                default:
                    break;
            }

            if (type == 1)
                return null;

            List<int> specifiedIds = new();
            while (specifiedIds.Count == 0)
            {
                string line = Console.ReadLine();
                if (type == 3)
                    line = File.ReadAllText(line);

                string[] splited = line.Split(',');
                foreach (string id in splited)
                    if (int.TryParse(id, out int number))
                        specifiedIds.Add(number);
                    else
                    {
                        Console.WriteLine("One or more specified ids was not a number");
                        specifiedIds.Clear();
                    }
            }
            Console.WriteLine();
            return specifiedIds;
        }

        private static int GetMinimalSupport()
        {
            Console.WriteLine("Set minimal support of frequent itemset.");
            int minimalSupport = 0;
            while (minimalSupport <= 0)
            {
                Console.WriteLine("Insert number greater then 0:");
                string line = Console.ReadLine();
                bool parsed = int.TryParse(line, out minimalSupport);

                if (!parsed)
                    continue;
            }

            Console.WriteLine();
            return minimalSupport;
        }

        private static string GetDatasetName()
        {
            Console.WriteLine("Remember that the dataset .txt files (1-3) have to be in the same folder as the application.");
            Console.WriteLine("Choose dataset.\n" +
                "1. Fruithut (181970 transactions, 1265 unique items)\n" +
                "2. Liquor_11 (9284 transactions, 2626 unique items)\n" +
                "3. Online Retail (541909 transactions, 2603 unique items)\n" +
                "4. Toy (8 transactions, 6 unique items)");

            int dataset = 0;
            while (dataset < 1 || dataset > 4)
            {
                Console.WriteLine("Insert number from 1 to 4:");
                string line = Console.ReadLine();
                bool parsed = int.TryParse(line, out dataset);

                if (!parsed)
                    continue;
            }

            Console.WriteLine();
            return dataset switch
            {
                1 => "Fruithut",
                2 => "Liquor",
                3 => "OnlineRetail",
                4 => "Toy",
                _ => throw new ArgumentException("Not recognized dataset name"),
            };
        }

        private static AlgorithmType GetAlgorithmType()
        {
            Console.WriteLine("Choose algorithm type.\n1. Eclat\n2. dEclat");
            int type = 0;
            while (type != 1 && type != 2)
            {
                Console.WriteLine("Insert number 1 or 2:");
                string line = Console.ReadLine();
                bool parsed = int.TryParse(line, out type);

                if (!parsed)
                    continue;
            }

            Console.WriteLine();
            return type == 1 ? AlgorithmType.Eclat : AlgorithmType.dEclat;
        }
    }
}
