﻿using Eclat_dEclat.Enums;
using Eclat_dEclat.Models;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eclat_dEclat.Algorithms
{
    public abstract class FIsAlgorithm
    {
        public AlgorithmStatistics Statistics { get; }
        public int TransactionsCount
        {
            get
            {
                if (transactionsWithItemsetsOfInteres != null)
                    return transactionsWithItemsetsOfInteres.Count;

                return 0;
            }
        }

        protected Dictionary<int, List<int>> transactionsItemsets;
        protected Dictionary<int, List<int>> transactionsWithItemsetsOfInteres;

        public FIsAlgorithm(Dictionary<int, List<int>> transactionsItemsets, List<int> itemsOfInterestIDs = null, AlgorithmStatistics statistics = null)
        {
            if (statistics == null)
                Statistics = new();
            else
                Statistics = statistics;

            this.transactionsItemsets = transactionsItemsets;
            if (itemsOfInterestIDs != null)
            {
                this.transactionsWithItemsetsOfInteres = CaluclateTransactionsWithItemsetsOfInteres(itemsOfInterestIDs);
                Statistics.DistinctItemsCount = itemsOfInterestIDs.Count;
                Statistics.SelectedItems = itemsOfInterestIDs;
            }
            else
            {
                this.transactionsWithItemsetsOfInteres = transactionsItemsets;
                Statistics.SelectedItems = null;
            }
        }

        protected abstract bool GenerateFirstLevels(Tree fiTree, int minSup);
        protected abstract Node ConcatenateNodes(Node node1, Node node2);


        public Tree GenerateFIs(int minSup)
        {
            Statistics.MinimalSupport = minSup;

            Tree fiTree = new();
            GenerateFITree(fiTree, minSup);

            return fiTree;
        }

        private void GenerateFITree(Tree fiTree, int minSup)
        {
            bool generated = GenerateFirstLevels(fiTree, minSup);

            if (generated)
                while (TryGenerateNextLevel(fiTree, minSup, ConcatenateNodes)) ;
        }

        protected bool TryGenerateNextLevel(Tree fiTree, int minSup, Func<Node, Node, Node> concatenateNodes)
        {
            Stopwatch levelGenerationTimer = new();
            levelGenerationTimer.Start();
            int levelNr = fiTree.AddLevel();
            Statistics.CreatedCandidatesCount[levelNr] = 0;
            Statistics.DiscoveredFrequentItemsetsCount[levelNr] = 0;
            bool generated = false;

            List<Node> prevLevel = fiTree.TreeNodes[levelNr - 1];

            for (int i = 0; i < prevLevel.Count; i++)
                for (int j = i + 1; j < prevLevel.Count; j++)
                {
                    Node concatenated = concatenateNodes(prevLevel[i], prevLevel[j]);
                    if (concatenated == null)
                        break;

                    Statistics.CreatedCandidatesCount[levelNr]++;
                    if (concatenated.NodeSupport > minSup)
                    {
                        fiTree.AddNode(concatenated);
                        Statistics.DiscoveredFrequentItemsetsCount[levelNr]++;
                        generated = true;
                    }
                }

            if (!generated)
            {
                fiTree.RemoveLastLevel();
                Statistics.DiscoveredFrequentItemsetsCount.Remove(levelNr);
            }

            levelGenerationTimer.Stop();
            Statistics.TreeLevelGenerationTime[levelNr] = levelGenerationTimer.Elapsed;

            return generated;
        }

        protected Dictionary<int, List<int>> CaluclateTransactionsWithItemsetsOfInteres(List<int> itemsOfInterestIDs) //function to rework
        {
            Dictionary<int, List<int>> transactionsWithItemsetsOfInteres = new();

            foreach (int key in transactionsItemsets.Keys)
            {
                foreach (int id in transactionsItemsets[key])
                {
                    if (itemsOfInterestIDs.Contains(id))
                    {
                        if (!transactionsWithItemsetsOfInteres.ContainsKey(key))
                            transactionsWithItemsetsOfInteres[key] = new List<int>();

                        transactionsWithItemsetsOfInteres[key].Add(id);
                    }
                }
            }

            return transactionsWithItemsetsOfInteres;
        }

        protected bool CanConcatenateNodes(Node node1, Node node2)
        {
            if (node1.ItemSetIDs[^1] == node2.ItemSetIDs[^1])
                return false;

            for (int i = 0; i < node1.ItemSetIDs.Count - 1; i++)
                if (node1.ItemSetIDs[i] != node2.ItemSetIDs[i])
                    return false;

            return true;
        }

        public static FIsAlgorithm CreateAlgorithmInstance(AlgorithmType algorithmType, Dictionary<int, List<int>> transactionsItemsets, List<int> itemsOfInterestIDs = null, AlgorithmStatistics statistics = null)
        {
            switch (algorithmType)
            {
                case AlgorithmType.Eclat:
                    return new Eclat(transactionsItemsets, itemsOfInterestIDs, statistics);
                case AlgorithmType.dEclat:
                    return new DEclat(transactionsItemsets, itemsOfInterestIDs, statistics);
                default:
                    return null;
            }
        }


    }
}
