﻿using Eclat_dEclat.Models;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eclat_dEclat.Algorithms
{
    public class DEclat : FIsAlgorithm
    {
        public DEclat(Dictionary<int, List<int>> transactionsItemsets, List<int> itemsOfInterestIDs = null, AlgorithmStatistics statistics = null) : base(transactionsItemsets, itemsOfInterestIDs, statistics) { }

        protected override bool GenerateFirstLevels(Tree fiTree, int minSup)
        {
            Stopwatch levelCreation = new();
            if (Statistics != null)
                levelCreation.Start();
            fiTree.AddLevel();
            Statistics.CreatedCandidatesCount[0] = 1;
            Statistics.DiscoveredFrequentItemsetsCount[0] = 1;
            fiTree.AddNode(new Node(new List<int>(), new List<int>(), transactionsWithItemsetsOfInteres.Keys.Count));
            if (Statistics != null)
            {
                levelCreation.Stop();
                Statistics.TreeLevelGenerationTime[0] = levelCreation.Elapsed;
                levelCreation.Restart();
            }

            fiTree.AddLevel();
            Statistics.CreatedCandidatesCount[1] = 0;
            Statistics.DiscoveredFrequentItemsetsCount[1] = 0;
            Dictionary<int, List<int>> oneElemenetItemssets = new();

            foreach (int transactionID in transactionsWithItemsetsOfInteres.Keys)
                foreach (int value in transactionsWithItemsetsOfInteres[transactionID])
                {
                    if (oneElemenetItemssets.ContainsKey(value))
                        oneElemenetItemssets[value].Add(transactionID);
                    else
                        oneElemenetItemssets[value] = new List<int>() { transactionID };
                }

            bool generated = false;
            foreach (int itemset in oneElemenetItemssets.Keys)
            {
                Statistics.CreatedCandidatesCount[1]++;
                if (oneElemenetItemssets[itemset].Count >= minSup)
                {
                    Statistics.DiscoveredFrequentItemsetsCount[1]++;
                    fiTree.AddNode(new Node(new List<int>() { itemset }, oneElemenetItemssets[itemset], oneElemenetItemssets[itemset].Count));
                    generated = true;
                }
            }

            if (!generated)
            {
                fiTree.RemoveLastLevel();
                Statistics.DiscoveredFrequentItemsetsCount.Remove(1);

                if (Statistics != null)
                {
                    levelCreation.Stop();
                    Statistics.TreeLevelGenerationTime[1] = levelCreation.Elapsed;
                }

                return generated;
            }
            else
            {
                if (Statistics != null)
                {
                    levelCreation.Stop();
                    Statistics.TreeLevelGenerationTime[1] = levelCreation.Elapsed;
                }
                return TryGenerateNextLevel(fiTree, minSup, ConcatenateTidToDiff);
            }
        }

        private Node ConcatenateTidToDiff(Node node1, Node node2)
        {
            if (!CanConcatenateNodes(node1, node2))
                return null;

            List<int> nodeItemset = new();
            nodeItemset.AddRange(node1.ItemSetIDs);
            nodeItemset.Add(node2.ItemSetIDs[^1]);

            List<int> nodeTransactions = node1.TransactionIDs.Except(node2.TransactionIDs).ToList();
            int nodeSupport = node1.NodeSupport - nodeTransactions.Count;

            return new Node(nodeItemset, nodeTransactions, nodeSupport);
        }

        protected override Node ConcatenateNodes(Node node1, Node node2)
        {
            if (!CanConcatenateNodes(node1, node2))
                return null;

            List<int> nodeItemset = new();
            nodeItemset.AddRange(node1.ItemSetIDs);
            nodeItemset.Add(node2.ItemSetIDs[^1]);

            List<int> nodeTransactions = node2.TransactionIDs.Except(node1.TransactionIDs).ToList();
            int nodeSupport = node1.NodeSupport - nodeTransactions.Count;

            return new Node(nodeItemset, nodeTransactions, nodeSupport);
        }
    }
}
