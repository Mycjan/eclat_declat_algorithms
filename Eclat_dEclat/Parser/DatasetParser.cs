﻿using Eclat_dEclat.Models;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Eclat_dEclat.Parser
{
    public static class DatasetParser
    {
        private static readonly Dictionary<int, List<int>> toyDataset = new()
        {
            { 0, new List<int>() { 1, 2, 3, 5 } },
            { 1, new List<int>() { 1, 2, 3, 5, 6 } },
            { 2, new List<int>() { 1, 2, 3, 8 } },
            { 3, new List<int>() { 1, 2, 5 } },
            { 4, new List<int>() { 1, 3, 6, 8 } },
            { 5, new List<int>() { 2, 5, 6 } },
            { 6, new List<int>() { 8 } },
            { 7, new List<int>() { 1, 6 } }
        };

        public static readonly Dictionary<string, (string path, int distinctElementsCount)> datasetsDictionary = new()
        {
            { "Fruithut", ("..\\..\\..\\fruithut_original.txt", 1265) },
            { "Liquor", ("..\\..\\..\\liquor_11frequent.txt", 2626) },
            { "OnlineRetail", ("..\\..\\..\\OnlineRetailZZ.txt", 2603) },
            { "Toy", (null, 6) },
        };

        public static Dictionary<int, List<int>> ParseDataset(string name, AlgorithmStatistics statistics = null)
        {
            Stopwatch datasetTimer = new();

            if (name == "Toy")
            {
                if (statistics != null)
                {
                    datasetTimer.Stop();
                    statistics.DatasetReadingTime = datasetTimer.Elapsed;
                }
                return toyDataset;
            }

            string path = datasetsDictionary[name].path;
            if (!File.Exists(path))
                path = Path.GetFileName(path);

            if (statistics != null)
                datasetTimer.Start();

            Dictionary<int, List<int>> parsedTransactions = new();

            FileStream fileStream = File.OpenRead(path);
            StreamReader reader = new StreamReader(fileStream);
            int tId = 0;
            string line;

            while ((line = reader.ReadLine()) != null)
            {
                if (line.Length == 0 || Regex.IsMatch(line, @"^\D"))
                    continue;

                string[] numbersToParse = line.Split(null);
                List<int> itemsIds = new();
                foreach (string id in numbersToParse)
                    itemsIds.Add(int.Parse(id));

                parsedTransactions[tId++] = itemsIds;
            }

            if (statistics != null)
            {
                datasetTimer.Stop();
                statistics.DatasetReadingTime = datasetTimer.Elapsed;
            }

            return parsedTransactions;
        }
    }
}
